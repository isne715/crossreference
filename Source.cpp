#include <iostream>
#include <fstream> //about input file
#include <string>
#include <sstream> //interoperation between stream and string.

#include "tree.h"

using namespace std;

int main() {
	Tree<string> testTree;
	ifstream file;
	string line, word;
	char count = '1';
	string fileUserChoose;

	cout << "What .txt file do you need to check line of each word? " << endl << "name of file(in form abdc.txt): ";
	cin >> fileUserChoose;

	file.open(fileUserChoose);

	if (file.is_open()) {
		while (getline(file, line)) {
			istringstream subLine(line);
			while (getline(subLine, word, ' ')) {
				word = word + " ------> in line: " + count;
				testTree.insert(word);
			}
			count++;
		}
		file.close();
	}
	else {
		cout << "CAN NOT OPEN THIS FILE!!!" << endl;
	}

	testTree.inorder();

	return 0;
}